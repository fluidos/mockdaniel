
cSRC += 


ccSRC +=  \
	main.cc \
	datamodel/BoolParameter.cc \
	datamodel/IntParameter.cc \
	datamodel/StringParameter.cc \
	datamodel/FloatParameter.cc \
	numeric/OneCurve.cc \
	numeric/Physics.cc \
	numeric/OneFluxFunction.cc \
	control/Dispatcher.cc \
	control/UI.cc \
	control/CmdlineUI.cc \
	control/XmlUI.cc \
	control/Control.cc \
	control/Command.cc \
	presentation/StringSettings.cc \
	presentation/UISpec.cc \
	presentation/ProfileSettings.cc \
	presentation/Configurable.cc \
	presentation/IntSettings.cc \
	presentation/IntProfile.cc \
	presentation/FloatProfile.cc \
	presentation/FloatSettings.cc \
	presentation/MacroMethod.cc \
	presentation/BoolSettings.cc \
	presentation/Profile.cc \
	presentation/CalcMethodSettings.cc \
	presentation/Settings.cc \
	presentation/AuxMethodSettings.cc \
	presentation/BoolProfile.cc \
	presentation/StringProfile.cc \
	presentation/PhysicsSettings.cc \
	presentation/MacroCalcMethod.cc \
	presentation/commands/CreateMacroCommand.cc \
	presentation/commands/CalcCommand.cc \
	presentation/commands/FloatSetCommand.cc \
	presentation/commands/IntSetCommand.cc \
	presentation/commands/AddToMacroCommand.cc \
	presentation/commands/StringSetCommand.cc \
	presentation/commands/BoolSetCommand.cc



#	control/rapidxml-1.13/main.cc \
#	profile/Layout.cc \
#	profile/TestPhysicsProfile.cc \

cppSRC += 


FSRC += 

