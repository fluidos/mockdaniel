#ifndef _AuxMethodSettings
#define _AuxMethodSettings

#include <Configurable.h>
#include <string>
#include <iostream>

using namespace std;


class AuxMethodSettings : public Configurable
{

public:
	AuxMethodSettings();
	void show(void);

};

#endif
