#ifndef _TestPhysicsProfile
#define _TestPhysicsProfile

#include <presentation>


class TestPhysicsProfile
{
private:
	Profile * profile_;

public:
	TestPhysicsProfile();
	ProfileSettings * profile();

};

#endif
