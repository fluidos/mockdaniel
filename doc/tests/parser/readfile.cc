/* 1 getline ( intut_stream, str, delim ); Extracts characters from intut_stream and stores them in str until s.max_size() characters have been extracted, the end of file occurs, or delim is encountered, in which case delim is extracted from istr but is not stored in s 2 getline( Iter, str ) Inputs a string value for str as in the preceding func­ tion with delim = */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

int main () 
{
    string str;
    /* Read a line terminated by '$'
    cout << "Enter string (EOL = $) : ";
    getline (cin, str, '$');
    cout << "Str is : " << str << endl;
    */

    ifstream In("data.dat");
    vector <string> v;

    //cout << endl << "Read data from file" << endl;
    while ( ! In.eof() )
    {
    	getline (In, str);
    	v.push_back(str);
    }

    //copy (v.begin(),v.end(), ostream_iterator(cout,"\n"));
    //cout << endl;
    for (int i=0; i<v.size()-1; i++)
        cout << i << " - " << v[i] << endl;
    	

    return 0;
}
