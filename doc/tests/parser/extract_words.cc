#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

// From: http://anaturb.net/C/string_exapm.htm

int main (void)
{
	string  str;
	int n;

	str = "daniel Lins de albquerque";
	n = str.find_first_of(" ",0);
	cout << "n = " << n << endl;

	str = "# daniel Lins de albquerque";
	n = str.find_first_of(" ",0);
	cout << "n = " << n << endl;

	n = str.find_first_of("daniel",1);
	cout << "n = " << n << endl;
}
	
